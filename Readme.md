WeeChat Scripts
===============

About
-----

A collection of scripts for [WeeChat][weechat].

Scripts
-------


Script         | Description
-------------- | ---------------------------------------------------------------------------------------------
`toshiba.py`   | A script which replaces all sent messages containing "toshiba" with a random Japanese company
`ellipsis.py`  | Replaces `...` with the correct unicode ellipsis character `…`

[weechat]: http://weechat.org
