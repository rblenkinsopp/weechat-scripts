# vim:fileencoding=utf-8
#
# Copyright (C) 2015 Robert Blenkinsopp <robert@blenkinsopp.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

SCRIPT_NAME    = "ellipsis"
SCRIPT_AUTHOR  = "Robert Blenkinsopp <robert@blenkinsopp.net>"
SCRIPT_VERSION = "1.0"
SCRIPT_LICENSE = "GPL3"
SCRIPT_DESC    = "Replaces ... with the correct unicode ellipsis character"

import_ok = True

try:
    import weechat as w
except ImportError:
    print("This script must be run under WeeChat.")
    print("Get WeeChat now at: http://www.weechat.org/")
    import_ok = False

try:
    import re
except ImportError as message:
    w.prnt("", "Missing package(s) for %s: %s" % (SCRIPT_NAME, message))
    import_ok = False

# Handle input and replace toshiba.
def replace_ellipsis(data, modifier, modifier_data, privmsg):
    if not privmsg.startswith("/") or privmsg.startswith("//"):
        #TODO(robb): String is a raw IRC message, we need to split the data part off to avoid a network called toshiba (unlikely).
        return re.sub(r"(?<!\.)\.\.\.(?!\.)", unichr(0x2026), privmsg).encode('utf-8')
    else:
        return privmsg

# Register the script.
if import_ok and w.register(SCRIPT_NAME, SCRIPT_AUTHOR, SCRIPT_VERSION, SCRIPT_LICENSE, SCRIPT_DESC, "", ""):
   w.hook_modifier("irc_out1_PRIVMSG", "replace_ellipsis", "")
   w.hook_modifier("input_text_for_buffer", "replace_ellipsis", "")
