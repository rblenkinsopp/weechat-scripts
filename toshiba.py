# vim:fileencoding=utf-8
#
# Copyright (C) 2015 Robert Blenkinsopp <robert@blenkinsopp.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

SCRIPT_NAME    = "toshiba"
SCRIPT_AUTHOR  = "Robert Blenkinsopp <robert@blenkinsopp.net>"
SCRIPT_VERSION = "1.0"
SCRIPT_LICENSE = "GPL3"
SCRIPT_DESC    = "Replaces toshiba with other Japanese companies"

import_ok = True

try:
    import weechat as w
except ImportError:
    print("This script must be run under WeeChat.")
    print("Get WeeChat now at: http://www.weechat.org/")
    import_ok = False

try:
    import re
    import random
except ImportError as message:
    w.prnt("", "Missing package(s) for %s: %s" % (SCRIPT_NAME, message))
    import_ok = False

WIKI_URL  = "https://en.wikipedia.org/wiki/List_of_companies_of_Japan"
USERAGENT = "Mozilla/5.0"

# Up-to-date as of 04/08/2015
COMPANIES = [
    u'77 Bank',
    u'81 Produce',
    u'Acom',
    u'Advantest',
    u'\xc6on',
    u'Aichi Bank',
    u'Aiful',
    u'Aisin Seiki',
    u'Aiwa',
    u'Ajinomoto',
    u'Akai',
    u'Akita Bank',
    u'All Nippon Airways',
    u'Allied Telesis',
    u'Alps Electric',
    u'Alpine Electronics',
    u'ANEST IWATA',
    u'Aniplex',
    u'AOC Holdings',
    u'Aomori Bank',
    u'Aoshima',
    u'Aoyagi Metals Company',
    u'Aozora Bank',
    u'Arai Helmet Ltd.',
    u'Arc System Works',
    u'Aruze',
    u'Asahi Breweries',
    u'Asahi Glass',
    u'Asahi Kasei',
    u'Asahi Shimbun',
    u'Astellas Pharma',
    u'Autobacs Seven Co.',
    u'Avex Group',
    u'Baby, The Stars Shine Bright',
    u'Bandai',
    u'Bandai Visual',
    u'Namco Bandai Games',
    u'Namco Bandai Holdings',
    u'Bank of Iwate',
    u'Bank of Tokyo-Mitsubishi UFJ',
    u'Bank of Yokohama',
    u'A Bathing Ape',
    u'Bones',
    u'Bridgestone',
    u'Brother Industries',
    u'Bungeishunj\u016b',
    u'Calsonic Kansei Corporation',
    u'Canon',
    u'Capcom',
    u'Casio',
    u'Cateye',
    u'Cave',
    u'Central Japan Railway Company',
    u'Chiba Bank',
    u'Chino corporation',
    u'Chisso',
    u'Chizu Express',
    u'Chubu Electric Power',
    u'Chugoku Electric Power',
    u'Chuokoron-Shinsha',
    u'Citizen',
    u'Cosmo Oil',
    u'Cospa',
    u'Credit Saison',
    u'CSK Holdings',
    u'Dai Nippon Printing',
    u'Daiei',
    u'Daihatsu',
    u'Daiichi Sankyo',
    u'Daikin Industries',
    u'Daikyo',
    u'Daimaru',
    u'Dainippon Sumitomo Pharma',
    u'Daiwa House Industry',
    u'Daiwa Securities',
    u'Data East',
    u'Denon',
    u'Denso',
    u'Dentsu',
    u'Digital Frontier',
    u'DIC Corp.',
    u'DMG Mori Seiki',
    u'Dome',
    u'Dream Stage Entertainment',
    u'East Japan Railway Company',
    u'Ebara Corporation',
    u'Eisai',
    u'Eizo',
    u'Electric Power Development Company',
    u'Ezaki Glico',
    u'Falken Tires',
    u'Fanuc',
    u'Fast Retailing',
    u'Fenrir Inc',
    u'Fuji Electric',
    u'Fuji Fire & Marine',
    u'Fuji Heavy Industries',
    u'Fuji Xerox Co., Ltd.',
    u'Fujifilm',
    u'Fuji Television',
    u'Fujitsu',
    u'Funai',
    u'Furukawa Electric',
    u'Furuno',
    u'Futaba Corporation',
    u'Gainax',
    u'GEOS',
    u'Trust Company Ltd.',
    u'GS Yuasa',
    u'Gonzo',
    u'H2O Retailing',
    u'Hamamatsu Photonics',
    u'Hankyu Hanshin Holdings',
    u'Hankyu Railway',
    u'Hanshin Electric Railway',
    u'Hasegawa',
    u'Hazama Ando',
    u'HI-LEX',
    u'Hirobo',
    u'Hisamitsu Pharmaceutical',
    u'Hitachi',
    u'HKS',
    u'Hokkaid\u014d Electric Power',
    u'Hokkaido Railway Company',
    u'Hokkoku Bank',
    u'Hokuriku Electric Power Company',
    u'Hokuriku Bank',
    u'Hokuto Bank',
    u'Honda',
    u'Horiba',
    u'Horipuro',
    u'Hosiden',
    u'Howa Machinery Company, Limited',
    u'Hoya Corporation',
    u'Hoyu Company, Limited',
    u'Human Entertainment',
    u'Idemitsu Kosan',
    u'IHI Corporation',
    u'Impul',
    u'Index Corporation',
    u'Inpex',
    u'Irem',
    u'Iris Ohyama',
    u'Isetan',
    u'Isuzu Motors',
    u'Ito-Yokado',
    u'Itochu',
    u'Iwataya Mitsukoshi',
    u'Izumiya',
    u'Jaleco',
    u'Japan Airlines',
    u'Japan Credit Bureau',
    u'Japan Freight Railway Company',
    u'Japan Industrial Partners, Inc.',
    u'Japan Post Bank',
    u'Japan Post Holdings',
    u'Japan Post Insurance',
    u'Japan Post Network',
    u'Japan Post Service',
    u'Japan Remote Control',
    u'Japan Tobacco',
    u'JEOL',
    u'JFE Holdings',
    u'Joyo Bank',
    u'JTEKT',
    u'JUN Auto',
    u'JVC (Victor Company of Japan, Ltd.)',
    u'K Line',
    u'Kadokawa Shoten',
    u'Kagome',
    u'Kajima Construction',
    u'Kaneka Corporation',
    u'Kanematsu',
    u'Kansai Electric Power Company',
    u'Kansai Paint',
    u'Kao Corporation',
    u'Kawai Musical Instruments Manufacturing',
    u'Kawasaki Heavy Industries',
    u'KDDI',
    u'Keihin Electric Express Railway',
    u'Keio Corporation',
    u'Keisei Electric Railway',
    u'Kenwood Electronics',
    u'Keyence',
    u'Kikkoman',
    u'Kintetsu',
    u'Kirin Brewery Company',
    u'Kita-Osaka Kyuko Railway',
    u'Kobe Electric Railway',
    u'Kobe New Transit',
    u'Kobe Steel',
    u'Komatsu Limited',
    u'Komori',
    u'Konami',
    u'Kondo Kagaku',
    u'Kong\u014d Gumi',
    u'Konica Minolta',
    u'Korg',
    u'Kubota',
    u'Kumagai Gumi',
    u'Kumon',
    u'Kuraray',
    u'Kyocera',
    u'KYB Corporation',
    u'Kyosho',
    u'Kyushu Electric Power',
    u'Kyushu Railway Company',
    u'Livedoor',
    u'Mabuchi Motor',
    u'Maeda Corporation',
    u'Makita',
    u'Mandom',
    u'Marubeni',
    u'Maruhon',
    u'Marui',
    u'Mazda',
    u'Mazdaspeed',
    u'Media Create',
    u'Meiji Holdings',
    u'Meiji Dairies',
    u'Meiji Seika',
    u'Meiji Yasuda Life Insurance',
    u'Melco Holdings Inc.',
    u'Metropolitan Intercity Railway Company',
    u'Michinoku Bank',
    u'Mikasa Corporation',
    u'Millea Holdings',
    u"MINE'S",
    u'Mister Donut',
    u'MITSUBA',
    u'Mitsubishi',
    u'Mitsubishi Motors',
    u'Mitsubishi Chemical',
    u'Mitsubishi Corporation',
    u'Mitsubishi Electric',
    u'Mitsubishi Estate',
    u'Mitsubishi Gas Chemical',
    u'Mitsubishi Heavy Industries',
    u'Mitsubishi Materials',
    u'Mitsubishi Pencil',
    u'Mitsubishi UFJ Financial Group',
    u'Mitsubishi UFJ Trust and Banking Corporation',
    u'Mitsubishi UFJ Securities Co., Ltd.',
    u'Mitsui',
    u'Mitsui Bussan',
    u'Mitsui Chemicals',
    u'Mitsui Engineering & Shipbuilding',
    u'Mitsui Fudosan',
    u'Mitsui O.S.K. Lines',
    u'Mitsui Sumitomo Insurance',
    u'Mitsui Trust Holdings',
    u'Mitsukoshi',
    u'Mitsumi Electric',
    u'Mitutoyo',
    u'Mizuho Financial Group',
    u'Mizuho Bank',
    u'Mizuho Corporate Bank',
    u'Mizuho Trust & Banking',
    u'Mizuho Information & Research Institute',
    u'Mizuno Corporation',
    u'Moodyz',
    u'Morinaga & Company',
    u'Moriwaki Engineering',
    u'Morozoff Ltd.',
    u'Mugen Motorsports',
    u'Mugen Seiki',
    u'Muji',
    u'Murata Manufacturing',
    u'Nachi-Fujikoshi',
    u'Nagoya Railroad',
    u'Nagisa Auto',
    u'Nakajima Aircraft Company',
    u'Namco',
    u'Nankai Electric Railway',
    u'Nanto Bank',
    u'NHK',
    u'NHK Spring',
    u'Nichicon',
    u'Nichirei',
    u'Nichiro',
    u'Nidec',
    u'Nippon Broadcasting System',
    u'Nippon Computer Systems',
    u'NEC',
    u'Nihon Bussan',
    u'Nikko Cordial',
    u'Nikon',
    u'Nintendo',
    u'Nippon Electric Glass',
    u'Nippon Express',
    u'Nippon Light Metal',
    u'Nippon Meat Packers',
    u'Nippon Mining',
    u'Nippon Oil',
    u'Nippon Paper Industries',
    u'Nippon Sheet Glass',
    u'Nippon Shinpan',
    u'Nippon Steel',
    u'Nippon Steel Trading',
    u'Nippon TV Network',
    u'Nippon Yusen',
    u'Nipro',
    u'Nismo',
    u'Nissan Motors',
    u'Nisshin Steel',
    u'Nissin Electric',
    u'Nissin Foods',
    u'Nitto Boseki',
    u'Nitto Denko',
    u'Nomura Holdings',
    u'Nomura Securities',
    u'Nomura Research Institute',
    u'Nose Railway',
    u'NSK (company)',
    u'NTN Corporation',
    u'NTT',
    u'NTT DoCoMo',
    u'Obayashi Corporation',
    u'Odakyu Electric Railway',
    u'Oji Paper Company',
    u'condoms',
    u'Oki Electric Industry',
    u'Okuma Corporation',
    u'Olympus',
    u'Omron',
    u'Onkyo',
    u'ONO',
    u'OPA co. ltd.',
    u'Orient',
    u'Oriental Land Company',
    u'Orix',
    u'O.S. Engines',
    u'Osaka Gas',
    u'Otsuka Pharmaceutical Co.',
    u'Panasonic',
    u'Penta-Ocean',
    u'Pentax',
    u'Pentel',
    u'Pilot Pen Corporation',
    u'Pioneer Corporation',
    u'Pokka Corporation',
    u'Prince Motor',
    u'Production I.G',
    u'Raizing',
    u'Rakuten',
    u'Ralliart',
    u'Rays Engineering',
    u'RE Amemiya',
    u'Renesas Electronics Corporation',
    u'Renown',
    u'Resona Holdings',
    u'Ricoh',
    u'Rinnai',
    u'Rohm',
    u'Rohto Pharmaceutical Co.',
    u'Roland Corporation',
    u'Rosso Corporation',
    u"Royce' Confect Company",
    u'Rubycon Corporation',
    u'Saitama Resona Bank',
    u'San-Ai Oil',
    u'Sanden',
    u'Santen Pharmaceutical',
    u'Sanrio',
    u'Sansui Electric',
    u'Sanyo',
    u'Sapporo Beer',
    u'Sanwa Denshi Corporation',
    u'multitesters',
    u'Sanwa Electronic Instrument Co., Ltd.',
    u'SARD',
    u'Secom',
    u'Sega',
    u'Sega Sammy Holdings',
    u'Seibu Railway',
    u'Seiko',
    u'Seiko Epson',
    u'Seikosha',
    u'Seiyu Group',
    u'Sekisui House',
    u'Seven & I Holdings Co.',
    u'Sharp Corporation',
    u'Shikoku Electric Power',
    u'Shikoku Railway Company',
    u'Shimano',
    u'Shimadzu Corporation',
    u'Shimizu Corporation',
    u'Shin-Etsu Chemical',
    u'Shin-Keisei Electric Railway',
    u'Shinano Kenshi Co., Ltd.',
    u'Shinsei Bank',
    u'Shiseido',
    u'Shizuoka Bank',
    u'Shoei',
    u'Shonai Bank',
    u'Showa Denko',
    u'Showa Shell Sekiyu',
    u'Shueisha',
    u'Sigma Corporation',
    u'SKY Perfect',
    u'SMC Corporation',
    u'SNK Playmore',
    u'Snow Brand Milk',
    u'Softbank',
    u'SoftBank Telecom',
    u'Sogo',
    u'Sojitz',
    u'Sompo Japan Insurance',
    u'Sony',
    u'Sony Music Entertainment Japan',
    u'Spoon Sports',
    u'Square Enix',
    u'Stanley Electric',
    u'Star Micronics',
    u'Studio Ghibli',
    u'Subaru',
    u'Subaru Tecnica International',
    u'Sumco',
    u'Sumitomo',
    u'Sumitomo Chemical',
    u'Sumitomo Electric',
    u'Sumitomo Heavy Industries',
    u'Sumitomo Metal Industries',
    u'Sumitomo Metal Mining',
    u'Sumitomo Mitsui Financial Group',
    u'Sumitomo Mitsui Banking Corporation',
    u'Sumitomo Realty & Development',
    u'Sumitomo Riko',
    u'Sumitomo Rubber Industries, Ltd.',
    u'Sumitomo Trust & Banking',
    u'Sunsoft',
    u'Sunrise',
    u'Suntory',
    u'SunTour',
    u'Suzuki',
    u'Sysmex',
    u'T&D Holdings',
    u'Tadano Limited',
    u'TAIHO PHARMACEUTICAL CO. LTD.',
    u'Taiheiyo Cement',
    u'Taisei Corporation',
    u'Taisho Pharmaceutical',
    u'Taito Corporation',
    u'Taiyo Yuden',
    u'Takashimaya',
    u'Takata Corporation',
    u'Takeda Pharmaceutical',
    u'Takefuji',
    u'Takenaka Corporation',
    u'Tamiya Corporation',
    u'Tamron',
    u'TDK Corporation',
    u'TEAC Corporation',
    u'Teijin',
    u'Tenyo',
    u'Terumo',
    u'Tiger Corporation',
    u'Toaplan',
    u'Tobu Railway',
    u'Toda Racing',
    u'Toei Animation',
    u'Toei Company',
    u'Toho',
    u'Toho Bank',
    u'Tohoku Bank',
    u'Tohoku Electric Power',
    u'Tokai Carbon',
    u'Tokaido',
    u'Tokio Marine & Nichido Fire Insurance',
    u'Tokuyama Corporation',
    u'Tokyo Broadcasting System',
    u'Tokyo Electric Power',
    u'Tokyo Electron',
    u'Tokyo Gas',
    u'Tokyu Corporation',
    u'Tokyo Tatemono',
    u'Tokyu Land',
    u'Tokyo Marui',
    u'Tokyo Metro',
    u'Tombow',
    u"TOM'S",
    u'Tomita Dream Sales',
    u'Tomy',
    u'Topcon',
    u'Toppan Printing',
    u'Toray',
    u'Toshiba',
    u'Tosoh',
    u'Toto',
    u'Toyo Rikagaku Kenkyusho',
    u'Toyo Seikan Kaisha',
    u'Toyo Tire & Rubber Company',
    u'Toyobo',
    u'Toyota',
    u'Toyota Boshoku',
    u'Toyota Industries',
    u'Toyota Tsusho',
    u'Treasure',
    u'Trend Micro',
    u'Trust Power',
    u'Tsubakimoto Chain',
    u'TV Asahi',
    u'TV Tokyo',
    u'UBE Industries',
    u'UCC Ueshima Coffee',
    u'Uniden',
    u'Unitika',
    u'Uniqlo',
    u'Uny',
    u'Veilside',
    u'Wacoal',
    u'Wacom',
    u'West Japan Railway',
    u'Work Wheels',
    u'Wako Pure Chemical Industries, Ltd',
    u'Yakult',
    u'Yamada Denki',
    u'Yamagata Bank',
    u'Yamaguchi Bank',
    u'Yamaha',
    u'Yamaha Motors',
    u'Yamasa',
    u'Yamato Transport',
    u'Yamazaki Baking',
    u'Yamazaki Mazak Corporation',
    u'Yanmar',
    u'Yaskawa Electric',
    u'YKK Group',
    u'Yokogawa Electric',
    u'Yokohama Rubber Company',
    u'Yokomo',
    u'Yoshimoto Kogyo',
    u'Yoshinoya',
    u'Zojirushi',
    u'Zuken',
]

BUFFER = ""

# Start and update of the companies database.
def start_companies_update():
    # See if we have the required components.
    try:
        from bs4 import BeautifulSoup
    except ImportError:
        w.prnt("", w.prefix("error") + SCRIPT_NAME + ": Requires BeautifulSoup4 for live updates. This can be installed with 'pip install beautifulsoup4'")
        return

    global BUFFER
    BUFFER = ""

    # Download the URL asyncronously.
    w.hook_process_hashtable("url:" + WIKI_URL, {"useragent": USERAGENT}, 10 * 1000, "progress_companies_update", "")

# In progress call back for the companies database.
def progress_companies_update(data, command, return_code, out, err):
    if return_code == w.WEECHAT_HOOK_PROCESS_ERROR:
        w.prnt("", w.prefix("error") + SCRIPT_NAME + ": Error downloading updated companies list")
        return w.WEECHAT_RC_OK

    global BUFFER

    if return_code == w.WEECHAT_HOOK_PROCESS_RUNNING:
        BUFFER += out
        return w.WEECHAT_RC_OK

    if return_code == 0:
        finish_companies_update(BUFFER)
        return w.WEECHAT_RC_OK
    
# Complete the companies database update.
def finish_companies_update(data):
    from bs4 import BeautifulSoup

    soup = BeautifulSoup(BUFFER, "html.parser")

    # Extract the table.
    tables = soup.find_all("table", { "class": "wikitable"})

    # Companies update.
    companies_update = []

    # Probably a more pythonic way to do this.
    for table in tables:
        for row in table.find_all("tr"):
            cols = row.find_all("td")

            try:
                if (len(cols) == 4):
                    col  = cols[0]
                    link = col.find("a")

                    # Deal with most cases...
                    if link is not None:
                        companies_update.append(link.text)

                    # Deal with the wierd ones which are not links.
                    else:
                        companies_update.append(col.text.splitlines()[0])
            except:
                continue
    
    # If something hasn't gone completely wrong, updated the list.
    if len(companies_update) > 20:
        global COMPANIES
        COMPANIES = companies_update
        w.prnt("", SCRIPT_NAME + ": Updated companies with " + str(len(companies_update)) + " entries")

# Handle input and replace toshiba.
def replace_toshiba(data, modifier, modifier_data, privmsg):
    if not privmsg.startswith("/") or privmsg.startswith("//"):
        #TODO(robb): String is a raw IRC message, we need to split the data part off to avoid a network called toshiba (unlikely).
        return re.sub(r"(?i)toshiba", lambda x: random.choice(COMPANIES), privmsg)
    else:
        return privmsg

# Update the companies database.
def command_toshiba_update(data, buffer, args):
    start_companies_update()
    return w.WEECHAT_RC_OK

# Register the script.
if import_ok and w.register(SCRIPT_NAME, SCRIPT_AUTHOR, SCRIPT_VERSION, SCRIPT_LICENSE, SCRIPT_DESC, "", ""):
    w.hook_modifier("input_text_for_buffer", "replace_toshiba", "")
    w.hook_modifier("irc_out1_PRIVMSG", "replace_toshiba", "")
    w.hook_command("toshiba_update", "Update the Japanese companies listing", "", "", "%", "command_toshiba_update", "")
    start_companies_update()

